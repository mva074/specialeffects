#ifndef MYCURVE_H
#define MYCURVE_H
#include <parametrics/curves/gmpline.h>
#include <core/types/gmpoint.h>


template <typename T, int n>
class Selector;

template <typename T>
class SelectorGridVisualizer;

template <typename T>
class MyCurve : public GMlib::PCurve<T,3>
{
    GM_SCENEOBJECT(MyCurve)
  public:
    MyCurve( const T a, const bool positive);
    MyCurve( const MyCurve<T>& copy );
    virtual ~MyCurve();

    //****************************************
    //****** Virtual public functions   ******
    //****************************************

    // from PCurve
    bool                isClosed() const override;
//    void                showSelectors(T rad = T(1), bool grid = true,
//                                      const GMlib::Color& selector_color = GMlib::GMcolor::darkBlue(),
//                                      const GMlib::Color& grid_color = GMlib::GMcolor::white()) override;

  protected:
    // Virtual functions from PCurve, which have to be implemented locally
    void                eval(T t, int d, bool l) const override;
    T                   getStartP() const override;
    T                   getEndP()   const override;

    // Protected data for the curve
    GMlib::Point<T,3>      _pt;
    GMlib::Vector<T,3>     _v;
    T                      _a;
    bool                   _positive;
    GMlib::Point<T,3>      _p1;
    GMlib::Point<T,3>      _p2;

    // Selectors and selector grid
    bool                         _selectors;        //!< Mark if we have selectors or not
    bool                         _grid;             //!< Mark if we have a selector grid or not
    SelectorGridVisualizer<T>*   _sgv;              //!< Selectorgrid
    std::vector<Selector<T,3>*>  _s;                //!< A set of selectors (spheres)
    T                            _selector_radius;
    GMlib::Color                 _selector_color;
    GMlib::Color                 _grid_color;

    GMlib::DVector<GMlib::Vector<T,3>>         _c;   //!< control points (control polygon)

  }; // END class PLine


#include "mycurve.c"
#endif // MYCURVE_H
