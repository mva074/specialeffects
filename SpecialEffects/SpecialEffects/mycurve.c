#include "mycurve.h"
#include <scene/selector/gmselector.h>

//*****************************************
// Constructors and destructor           **
//*****************************************

   /*! Default constructor to create a spiral using the parameter a.
    *
    *  \param[in] a         Spiral's size
    *  \param[in] positive  If is set to true the domain is between 0 and 1, otherwise it is between 0 and -1
    */

    template <typename T>
    inline
    MyCurve<T>::MyCurve( const T a, const bool positive) : GMlib::PCurve<T, 3> (20, 3, 7), _a(a), _positive(positive)
    {

    };


  /*! PLine<T>::PLine(const PLine<T>& copy )
   *  A copy constructor
   *  Making a copy of the curve (line)
   *
   *  \param[in] copy The curve to copy
   */
  template <typename T>
  inline
  MyCurve<T>::MyCurve( const MyCurve<T>& copy ) : GMlib::PCurve<T,3>(copy) {}



  /*! PLine<T>::~PLine()
   *  The destructor
   *  clean up and destroy all private data
   */
  template <typename T>
  MyCurve<T>::~MyCurve() {}


  //***************************************************
  // Overrided (public) virtual functons from PCurve **
  //***************************************************

  /*! bool PLine<T>::isClosed() const
   *  To tell that this curve (line) is always open.
   *
   */
  template <typename T>
  bool MyCurve<T>::isClosed() const {
      return false;
  }

//  template <typename T>
//  void MyCurve<T>::showSelectors( T rad, bool grid,
//                                  const GMlib::Color& selector_color, const GMlib::Color& grid_color ) {

//      if( !_selectors ) {
//          _s.resize( _c.getDim() );

//          for( int i = 0; i < _c.getDim(); i++ )
//              if(this->isScaled())
//                  this->insert( _s[i] = new Selector<T,3>( _c[i], i, this, this->_scale.getScale(), rad, selector_color ) );
//              else
//                  this->insert( _s[i] = new Selector<T,3>( _c[i], i, this, rad, selector_color ) );
//          _selectors       = true;
//      }
//      _selector_radius = rad;
//      _selector_color  = selector_color;

//      if( grid ) {
//          if(!_sgv) _sgv = new SelectorGridVisualizer<T>;
//          _sgv->setSelectors( _c, 0, false);
//          _sgv->setColor( grid_color );
//          GMlib::SceneObject::insertVisualizer( _sgv );
//          this->setEditDone();
//      }
//      _grid_color      = grid_color;
//      _grid            = grid;
//  }


  //******************************************************
  // Overrided (protected) virtual functons from PCurve **
  //******************************************************

  /*! void PCircle<T>::eval( T t, int d, bool l ) const
   *  Evaluation of the curve at a given parameter value
   *  To compute position and d derivatives at parameter value t on the curve.
   *  7 derivatives are implemented
   *
   *  \param  t[in]  The parameter value to evaluate at
   *  \param  d[in]  The number of derivatives to compute
   *  \param  l[in]  (dummy) because left and right are always equal
   */
  template <typename T>
  void MyCurve<T>::eval( T t, int d, bool /*l*/ ) const {

      t = 20*t;

    this->_p.setDim( d + 1 );

    this->_p[0][0] = _a * (cos(t) + t * sin(t));
    this->_p[0][1] = _a * (sin(t) - t * cos(t));
    this->_p[0][2] = t/3;


//    this->_p[0] = _p1 + sin(t*t)*_p2 + (t)*_p3;
//    this->_p[1] = _pt * 2 * t - sin(t*8) * _v;


    if( this->_dm == GMlib::GM_DERIVATION_EXPLICIT ) {
      if( d ) {
          this->_p[1][0] = _a * t * cos(t);
          this->_p[1][1] = _a * t * sin(t);
          this->_p[1][2] = T(1);
      }
      if( d > 1 ) {
          this->_p[2][0] = _a * (cos(t) + t * sin(t));
          this->_p[2][1] = _a * (sin(t) - t * cos(t));
          this->_p[2][2] = T(0);
      }
      if( d > 2 ) this->_p[3] = GMlib::Vector<T,3>(T(0));
      if( d > 3 ) this->_p[4] = GMlib::Vector<T,3>(T(0));
      if( d > 4 ) this->_p[5] = GMlib::Vector<T,3>(T(0));
      if( d > 5 ) this->_p[6] = GMlib::Vector<T,3>(T(0));
      if( d > 6 ) this->_p[7] = GMlib::Vector<T,3>(T(0));
    }
  }



  /*! T PLine<T>::getStartP() const
   *  Provides the start parameter value associated with
   *  the eval() function implemented above.
   *  (the start parameter value = 0).
   *
   *  \return The parametervalue at start of the internal domain
   */
  template <typename T>
  T MyCurve<T>::getStartP() const {
    return T(0);
  }



  /*! T PLine<T>::getEndP() const
   *  Provides the end parameter value associated with
   *  the eval() function implemented above.
   *  (the end parameter value = 1).
   *
   *  \return The parametervalue at end of the internal domain
   */
  template <typename T>
  T MyCurve<T>::getEndP() const {
      if (_positive == true)
          return T(1);
      else {
          return T(-1);
      }
  }

