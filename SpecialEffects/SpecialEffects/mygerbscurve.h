#ifndef MYGERBSCURVE_H
#define MYGERBSCURVE_H

#include <parametrics/gmpcurve.h>
#include <parametrics/gmpsubcurve>
#include "myblendedcurve.h"

template <typename T>
class MyGERBScurve : public GMlib::PCurve<T,3>
{
    GM_SCENEOBJECT(MyGERBScurve)

public:
    MyGERBScurve( GMlib::PCurve<T,3>* curve, int n);
    MyGERBScurve( GMlib::PCurve<T,3>* curve, std::vector<T> knot);
    ~MyGERBScurve();
    bool isClosed() const override;

protected:
    void    eval(T t, int d, bool l) const override;
    T       getStartP() const override;
    T       getEndP() const override;
    void    localSimulate(double dt) override;

private:
    void    makeKnotVector();
    void    makeControlCurves();
    int     findI(T t) const;
    T       findW( const int& d, const int& i, T t) const;
    T       computeBFunction(T t) const;

    const int _d = 1;
    const int _k = _d + 1;
    int _n;
    bool _knotPassed;
    std::vector<T> _t; //knot vector
    GMlib::DVector<GMlib::PCurve<T,3>*> _cc;
    GMlib::PCurve<T,3>* _modelCurve;
    GMlib::DVector<GMlib::Vector<T,3>> _initPos;

};

#include "mygerbscurve.c"
#endif // MYGERBSCURVE_H
