#ifndef MYGERBSSURFACE_H
#define MYGERBSSURFACE_H

#include <parametrics/gmpcurve.h>
#include <parametrics/gmpsubcurve>
#include "myblendedcurve.h"
#include "simplesubsurf.h"

template <typename T>
class MyGERBSSurface: public GMlib::PSurf<T,3>
{
    GM_SCENEOBJECT(MyGERBSSurface)

public:
    MyGERBSSurface(GMlib::PSurf<T,3>* surface, int n, int m);
    MyGERBSSurface(GMlib::PSurf<T,3>* surface, std::vector<T> uKnot, std::vector<T> vKnot);
    ~MyGERBSSurface();
    bool isClosedU() const override;
    bool isClosedV() const override;

protected:
    void    eval(T u, T v, int d1, int d2, bool lu, bool lv) const override;
    T       getStartPU() const override;
    T       getEndPU() const override;
    T       getStartPV() const override;
    T       getEndPV() const override;
    void    localSimulate(double dt) override;

private:
    void    makeKnotVectors();
    void    makeControlSurfaces();
    int     findI(T u) const;
    int     findJ(T v) const;
    T       findW(const int& d, const int& i, T t,
                  const std::vector<T>& _t) const;
    T       computeBFunction(T t, bool isUVector) const;

    const int _d = 1;
    const int _k = _d + 1;
    int _n;
    int _m;
    bool _knotsPassed;
    std::vector<T> _u, _v;//knot vectors
    GMlib::DMatrix<GMlib::PSurf<T,3>*> _cs;
    GMlib::PSurf<T,3>* _modelSurface;
//    GMlib::DVector<GMlib::Vector<T,3>> _initPos;

};

#include "mygerbssurface.c"
#endif // MYGERBSSURFACE_H
