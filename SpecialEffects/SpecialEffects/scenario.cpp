
#include <iostream>

#include "scenario.h"
#include "mybsplinecurve.h"
#include "mybspline.h"
#include "mycurve.h"
#include "myblendedcurve.h"
#include "mygerbscurve.h"
#include "mygerbssurface.h"

//// hidmanager
//#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <gmCoreModule>
#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>

// qt
#include <QQuickItem>


template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << v.size() << std::endl;
  for(uint i=0; i<v.size(); i++) out << " " << v[i];
  out << std::endl;
  return out;
}



void Scenario::initializeScenario() {

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8f, 0.002f, 0.0008f);
  this->scene()->insertLight( light, false );

  // Insert Sun
  this->scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3> init_cam_pos(  0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  0.0f, 0.0f, 1.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Front cam
  auto front_rcpair = createRCPair("Front");
  front_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, -50.0f, 0.0f ), init_cam_dir, init_cam_up );
  front_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( front_rcpair.camera.get() );
  front_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Side cam
  auto side_rcpair = createRCPair("Side");
  side_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( -50.0f, 0.0f, 0.0f ), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ), init_cam_up );
  side_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( side_rcpair.camera.get() );
  side_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  // Top cam
  auto top_rcpair = createRCPair("Top");
  top_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, 0.0f, 50.0f ), -init_cam_up, init_cam_dir );
  top_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  scene()->insertCamera( top_rcpair.camera.get() );
  top_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

/*/
      auto myCircle = new GMlib::PCircle<float>();
      myCircle->toggleDefaultVisualizer();
      myCircle->setColor(GMlib::GMcolor::blue());
      myCircle->setRadius(5);
      myCircle->showSelectors(0.5);
      myCircle->sample(100,4);
      this->scene()->insert(myCircle);
//*/
      GMlib::DVector< GMlib::Vector<float,3> > cp(8);
      cp[0]= GMlib::Vector<float,3>(0,0,0); cp[1]= GMlib::Vector<float,3>(1,1,0);
      cp[2]= GMlib::Vector<float,3>(2,0,2); cp[3]= GMlib::Vector<float,3>(3,2,0);
      cp[4]= GMlib::Vector<float,3>(4,1,0); cp[5]= GMlib::Vector<float,3>(5,1,-2);
      cp[6]= GMlib::Vector<float,3>(6,2,0); cp[7]= GMlib::Vector<float,3>(7,0,0);

      GMlib::DVector< GMlib::Vector<float,3> > cp2(8);
      cp2[0]= GMlib::Vector<float,3>(1,0,0); cp2[1]= GMlib::Vector<float,3>(1,1,1);
      cp2[2]= GMlib::Vector<float,3>(4,0,1); cp2[3]= GMlib::Vector<float,3>(5,2,0);
      cp2[4]= GMlib::Vector<float,3>(4,-3,0); cp2[5]= GMlib::Vector<float,3>(-5,1,-2);
      cp2[6]= GMlib::Vector<float,3>(6,2,-5); cp2[7]= GMlib::Vector<float,3>(7,1,1);
/*/
      auto newBSpline = new splineSpace::myBSpline<float>(cp, 5);
      newBSpline->toggleDefaultVisualizer();
      newBSpline->setColor(GMlib::GMcolor::blueViolet());
      newBSpline->showSelectors(0.5);
      newBSpline->sample(100,4);
      this->scene()->insert(newBSpline);

      auto newBSpline2 = new splineSpace::myBSpline<float>(cp2, 5);
      newBSpline2->toggleDefaultVisualizer();
      newBSpline2->setColor(GMlib::GMcolor::green());
      newBSpline2->showSelectors(0.5);
      newBSpline2->sample(100,4);
      this->scene()->insert(newBSpline2);
//*/
      auto mySpiral = new MyCurve<float>(0.1f, false);
      mySpiral->toggleDefaultVisualizer();
      mySpiral->setColor(GMlib::GMcolor::red());
      mySpiral->showSelectors(0.5);
      mySpiral->sample(100,4);
/*/
      this->scene()->insert(mySpiral);
//*/
      auto mySpiral2 = new MyCurve<float>(0.1f, true);
      mySpiral2->toggleDefaultVisualizer();
      mySpiral2->setColor(GMlib::GMcolor::red());
      mySpiral2->showSelectors(0.5);
      mySpiral2->sample(100,4);
/*/
      this->scene()->insert(mySpiral2);
//*/
      auto blendedCurve = new myBlendedCurve<float>(mySpiral, mySpiral2, 1);
      blendedCurve->toggleDefaultVisualizer();
      blendedCurve->setColor(GMlib::GMcolor::gold());
      blendedCurve->showSelectors(0.5);
      blendedCurve->sample(100,4);
/*/
      this->scene()->insert(blendedCurve);
//*/
//*/
      auto gerbsCurve = new MyGERBScurve<float>(blendedCurve, 4);
      gerbsCurve->toggleDefaultVisualizer();
      gerbsCurve->setColor(GMlib::GMcolor::green());
      //gerbsCurve->showSelectors(0.5);
      gerbsCurve->sample(100,4);
      this->scene()->insert(gerbsCurve);
//*/
//*/
      auto surface = new GMlib::PCylinder<float>(3, 4, 6);
      auto gerbsCylinder = new MyGERBSSurface<float>(surface, 5, 7);
      gerbsCylinder->toggleDefaultVisualizer();
      gerbsCylinder->setMaterial(GMlib::GMmaterial::snow());
      gerbsCylinder->replot(10, 10, 1, 1);
      this->scene()->insert(gerbsCylinder);
      gerbsCylinder->setVisible(false);
//*/
}

void Scenario::cleanupScenario() {

}

void Scenario::callDefferedGL() {

  GMlib::Array<const GMlib::SceneObject*> e_obj;
  this->scene()->getEditedObjects(e_obj);

  for(int i=0; i < e_obj.getSize(); i++)
    if(e_obj(i)->isVisible()) e_obj(i)->replot();
}


