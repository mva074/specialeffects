#include "mygerbssurface.h"

template<typename T>
MyGERBSSurface<T>::MyGERBSSurface(GMlib::PSurf<T, 3> *surface, int n, int m) :
    _modelSurface(surface), _n(n), _m(m), _knotsPassed(false)
{
    makeKnotVectors();
    makeControlSurfaces();
}

template<typename T>
MyGERBSSurface<T>::MyGERBSSurface( GMlib::PSurf<T, 3> *surface, std::vector<T> uKnot, std::vector<T> vKnot) :
    _modelSurface(surface), _v(vKnot), _u(uKnot), _knotsPassed(true),
    _n(uKnot.size()-_k-1), _m(vKnot.size()-_k-1)
{
    makeControlSurfaces();
}

template<typename T>
MyGERBSSurface<T>::~MyGERBSSurface()
{

}

template<typename T>
bool MyGERBSSurface<T>::isClosedU() const
{
    return _modelSurface->isClosedU();
}

template<typename T>
bool MyGERBSSurface<T>::isClosedV() const
{
    return _modelSurface->isClosedV();
}

template<typename T>
void MyGERBSSurface<T>::eval(T u, T v, int d1, int d2, bool /*lu*/, bool /*lv*/) const
{
    this->_p.setDim(d1 + 1, d2 + 1);
    int i = findI(u);
    int j = findJ(v);

    T bi = computeBFunction(u, true);
    T bj = computeBFunction(v, false);

    GMlib::DMatrix<GMlib::Vector<T,3>> p1 = ((1 - bi) * (1 - bj)) * _cs(i - 1)(j - 1)->evaluateParent(u, v, d1, d2);
    auto p2 = ((1 - bi) * bj) * _cs(i - 1)(j)->evaluateParent(u, v, d1, d2);
    auto p3 = (bi * (1 - bj)) * _cs(i)(j - 1)->evaluateParent(u, v, d1, d2);
    auto p4 = (bi * bj) * _cs(i)(j)->evaluateParent(u, v, d1, d2);

    this->_p = p1 + p2 + p3 + p4;
}

template<typename T>
T MyGERBSSurface<T>::getStartPU() const
{
    return _u[1];
}

template<typename T>
T MyGERBSSurface<T>::getEndPU() const
{
    return _u[_n+_d];
}

template<typename T>
T MyGERBSSurface<T>::getStartPV() const
{
    return _v[1];
}

template<typename T>
T MyGERBSSurface<T>::getEndPV() const
{
    return _v[_m+_d];
}

template<typename T>
void MyGERBSSurface<T>::localSimulate(double dt)
{
    for (int i = 0; i < _n+1; i++) {
        for (int j = 0; j < _m+1; j++) {
            if ((i + j)%2 == 0) {
                _cs[i][j]->rotate(dt, GMlib::Vector<T,3>(1, 0, 1));
                _cs[i][j]->setMaterial(GMlib::GMmaterial::pearl());
            }
        }
    }
}

template<typename T>
void MyGERBSSurface<T>::makeKnotVectors()
{
    if (_knotsPassed)
        return;

    auto du = _modelSurface->getParDeltaU() / _n;
    auto dv = _modelSurface->getParDeltaV() / _m;
    _u.resize(_n+_k+1);
    _v.resize(_m+_k+1);

    // building u vector
    for (int i = 0; i < _k; i++) {
        _u[i] = _modelSurface->getParStartU();
    }

    for (int i = _k; i <= _n; i++) {
        _u[i] = _modelSurface->getParStartU() + (i-_d)*du;
    }

    for (int i = _n+1; i < _n+_k+1; i++) {
        _u[i] = _modelSurface->getParEndU();
    }

    // building v vector
    for (int i = 0; i < _k; i++) {
        _v[i] = _modelSurface->getParStartV();
    }

    for (int i = _k; i <= _m; i++) {
        _v[i] = _modelSurface->getParStartV() + (i-_d)*dv;
    }

    for (int i = _m+1; i < _m+_k+1; i++) {
        _v[i] = _modelSurface->getParEndV();
    }

    if (isClosedU()){
        _u[0] = _u[1] - (_u[_n] - _u[_n-1]);
        _u[_n+1] = _u[_n] + _u[2] - _u[1];
    }
    if (isClosedV()) {
        _v[0] = _v[1] - (_v[_m] - _v[_m-1]);
        _v[_m+1] = _v[_m] + _v[2] - _v[1];
    }
}

template<typename T>
void MyGERBSSurface<T>::makeControlSurfaces()
{
    int n = _n + 1;
    int m = _m + 1;
    _cs.setDim(n, m);
    //_initPos.setDim(n);

    for (int i = 0; i < _n; i++) {
        for (int j = 0; j < _m; j++) {
            _cs[i][j] = new PSimpleSubSurf<T>(_modelSurface, _u[i], _u[i+2], _u[i+1], _v[j], _v[j+2], _v[j+1]);
            this->insert(_cs[i][j]);
            _cs[i][j]->toggleDefaultVisualizer();
            _cs[i][j]->replot(10,10,1,1);
            //_initPos[i] = _modelCurve->getPosition(_t[i+1]);
            //_cs[i][j]->setCollapsed(true);
        }
        if (isClosedV()) {
            _cs[i][_m] = _cs[i][0];
        }
        else {
            _cs[i][_m] = new PSimpleSubSurf<T>(_modelSurface, _u[i], _u[i+2], _u[i+1], _v[_m], _v[_m+2], _v[_m+1]);
            this->insert(_cs[i][_m]);
            _cs[i][_m]->toggleDefaultVisualizer();
            _cs[i][_m]->replot(10,10,1,1);
            //_cs[i][_m]->setCollapsed(true);
        }
    }

    for (int j = 0; j <= _m; j++) {
        if (isClosedU()) {
            _cs[_n][j] = _cs[0][j];
        }
        else {
            _cs[_n][j] = new PSimpleSubSurf<T>(_modelSurface, _u[_n], _u[_n+2], _u[_n+1], _v[j], _v[j+2], _v[j+1]);
            this->insert(_cs[_n][j]);
            _cs[_n][j]->toggleDefaultVisualizer();
            _cs[_n][j]->replot(10,10,1,1);
            //_cs[_n][j]->setCollapsed(true);
        }
    }

}


template<typename T>
int MyGERBSSurface<T>::findI(T u) const
{
    int i;
    for (i = 0; i < _u.size(); i++) {
        if (_u[i] > u) {
            break;
        }
    }

    if (i > _n+1)
        i = _n+1;

    i--;

    return i;
}

template<typename T>
int MyGERBSSurface<T>::findJ(T v) const
{
    int j;
    for (j = 0; j < _v.size(); j++) {
        if (_v[j] > v) {
            break;
        }
    }

    if (j > _m+1)
        j = _m+1;

    j--;

    return j;
}

template<typename T>
T MyGERBSSurface<T>::findW(const int& d, const int& i, T t,
                           const std::vector<T>& _t) const
{
    return (t - _t[i]) / (_t[i+d] - _t[i]);
}

template<typename T>
T MyGERBSSurface<T>::computeBFunction(T t, bool isUVector) const
{
    if (isUVector) {
        t = findW(_d, findI(t), t, _u);
    }
    else {
        t = findW(_d, findJ(t), t, _v);
    }

    return std::pow(t, 2) / (std::pow(t, 2) + std::pow((1-t), 2));
}




