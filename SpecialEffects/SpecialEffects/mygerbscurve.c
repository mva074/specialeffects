#include "mygerbscurve.h"

template<typename T>
MyGERBScurve<T>::MyGERBScurve(GMlib::PCurve<T, 3> *curve, int n) :
    _modelCurve(curve), _n(n), _knotPassed(false)
{
    makeKnotVector();
    makeControlCurves();
}

template<typename T>
MyGERBScurve<T>::MyGERBScurve(GMlib::PCurve<T, 3> *curve, std::vector<T> knot) :
    _modelCurve(curve), _t(knot), _knotPassed(true), _n(knot.size()-_k-1)
{
    makeControlCurves();
}

template<typename T>
MyGERBScurve<T>::~MyGERBScurve()
{

}

template<typename T>
bool MyGERBScurve<T>::isClosed() const
{
    return _modelCurve->isClosed();
}

template<typename T>
void MyGERBScurve<T>::eval(T t, int d, bool /*l*/) const
{
    this->_p.setDim( d + 1 );
    int i = findI(t);

    this->_p = (1 - computeBFunction(t)) * _cc[i - 1]->evaluateParent(t, d)
            + computeBFunction(t) * _cc[i]->evaluateParent(t, d);
}

template<typename T>
T MyGERBScurve<T>::getStartP() const
{
    return T(-1);
}

template<typename T>
T MyGERBScurve<T>::getEndP() const
{
    return T(1);
}

template<typename T>
void MyGERBScurve<T>::makeKnotVector()
{
    if (_knotPassed)
        return;

    auto dt = _modelCurve->getParDelta() / _n;
    _t.resize(_n+_k+1);
    for (int i = 0; i < _k; i++) {
        _t[i] = _modelCurve->getParStart();
    }

    for (int i = _k; i <= _n; i++) {
        _t[i] = _modelCurve->getParStart() + (i-_d)*dt;
    }

    for (int i = _n+1; i < _n+_k+1; i++) {
        _t[i] = _modelCurve->getParEnd();
    }

    if (isClosed()){
        _t[0] = _t[1] - (_t[_n] - _t[_n-1]);
        _t[_n+1] = _t[_n] + _t[2] - _t[1];
    }

}

template<typename T>
void MyGERBScurve<T>::makeControlCurves()
{
    int n = _n + 1;
    _cc.setDim(n);
    _initPos.setDim(n);

    for (int i = 0; i < _n; i++) {
        _cc[i] = new GMlib::PSubCurve<T>(_modelCurve, _t[i], _t[i+2], _t[i+1]);
        this->insert(_cc[i]);
        _cc[i]->toggleDefaultVisualizer();
        _cc[i]->sample(50,0);
        _initPos[i] = _modelCurve->getPosition(_t[i+1]);
        _cc[i]->setCollapsed(true);
    }

    if (isClosed()) {
        _cc[_n] = _cc[0];
        _initPos[_n] = _modelCurve->getPosition(_t[1]);
        _cc[_n]->setCollapsed(true);
    }
    else {
        _cc[_n] = new GMlib::PSubCurve<T>(_modelCurve, _t[_n], _t[_n+_k], _t[_n+_d]);
        this->insert(_cc[_n]);
        _cc[_n]->toggleDefaultVisualizer();
        _cc[_n]->sample(50,0);
        _initPos[_n] = _modelCurve->getPosition(_t[_n+1]);
        _cc[_n]->setCollapsed(true);
    }

}

template<typename T>
int MyGERBScurve<T>::findI(T t) const
{
    int i;
    for (i = 0; i < _t.size(); i++) {
        if (_t[i] > t) {
            break;
        }
    }

    if (i > _n+1)
        i = _n+1;

    i--;

    return i;
}

template<typename T>
T MyGERBScurve<T>::findW(const int& d, const int& i, T t) const
{
    return (t - _t[i]) / (_t[i+d] - _t[i]);
}

template<typename T>
T MyGERBScurve<T>::computeBFunction(T t) const
{
    t = findW(_d, findI(t), t);
    return std::pow(t, 2) / (std::pow(t, 2) + std::pow((1-t), 2));
}

template<typename T>
void MyGERBScurve<T>::localSimulate(double dt)
{

    auto u = GMlib::Vector<float,3>(cos(45), sin(45), 0.0f);
    auto v = GMlib::Vector<float,3>(-sin(45), cos(45), 0.0f);

    static double ddt = 0;
    GMlib::Vector<T,3> a;
    static bool sign = false;
    ddt+=dt;
    if (ddt > 0.7){
        sign = !sign;
        ddt = 0;
    }

    for(int i = 0; i < _cc.getDim(); i++) {

        if (sign) {
            _cc[i]->translate(dt*_initPos[i]);
        }
        else {
            _cc[i]->translate(-dt*_initPos[i]);
        }
        _cc[i]->rotateGlobal(dt, u, v);
    }
    this->sample(200, 0);
}

