#ifndef MYBLENDEDCURVE_H
#define MYBLENDEDCURVE_H
#include <parametrics/gmpcurve.h>

template <typename T>
class myBlendedCurve : public GMlib::PCurve<T,3>
{
    GM_SCENEOBJECT(myBlendedCurve)

    public:
    myBlendedCurve(GMlib::PCurve<T,3>* c0, GMlib::PCurve<T,3>* c1, T dt);
    ~myBlendedCurve();

protected:
  void                eval(T t, int d, bool l) const override;
  T                   getStartP() const override;
  T                   getEndP()   const override;

private:
  GMlib::PCurve<T, 3>* _c0; // curve 1
  GMlib::PCurve<T, 3>* _c1; // curve 2
  int _d; // degree
  int _k; // order
  T _dt; // where to blend between 0 and 1

  T blend(T t) const;
  GMlib::DVector<GMlib::Vector<T,3>>& getPosBlend(T t) const;

};

#include "myblendedcurve.c"
#endif // MYBLENDEDCURVE_H
