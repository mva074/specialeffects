#ifndef MYBSPLINE_H
#define MYBSPLINE_H

#include <parametrics/gmpcurve.h>

namespace splineSpace {

  template <typename T>
  class myBSpline : public GMlib::PCurve<T,3>
  {
  GM_SCENEOBJECT(myBSpline)

  public:
    myBSpline( const GMlib::DVector<GMlib::Vector<T,3>>& c);
    myBSpline( const GMlib::DVector<GMlib::Vector<T,3>>& p, int n);
    ~myBSpline();

  protected:
    void            eval(T t, int d, bool l) const override;
    T               getStartP() const override;
    T               getEndP()   const override;

  private:
    int                     findI(T t) const;
    T                       findW(const int& d, const int& findI, T t) const;
    GMlib::Vector<T, 4>     computeBasis(T t, int& i) const;
    void                    makeKnotVector(int n);
    void                    makeControlPoints(const GMlib::DVector<GMlib::Vector<T,3>> &p);


    GMlib::DVector<GMlib::Vector<T,3>> _c;
    std::vector<T> _t; // knot vector

    const int _d = 3;
    const int _k = _d + 1;
    int _n;
    bool _setPoint = true;
  };

}
// Include mypcircle class function implementations
#include "mybspline.c"

#endif // MYBSPLINE_H
