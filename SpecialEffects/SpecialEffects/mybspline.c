#include "mybspline.h"
#include <QtDebug>

namespace splineSpace {

    template<typename T>
    myBSpline<T>::myBSpline(const GMlib::DVector<GMlib::Vector<T, 3> > &c) : _c(c), _n(c.getDim())
    {
        makeKnotVector(c.getDim());
    }

    template<typename T>
    myBSpline<T>::myBSpline(const GMlib::DVector<GMlib::Vector<T, 3> > &p, int n) : _n(n), _setPoint(false)
    {
        makeKnotVector(n);
        makeControlPoints(p);

    }

    template<typename T>
    myBSpline<T>::~myBSpline() {}

    template<typename T>
    void myBSpline<T>::eval(T t, int d, bool l) const
    {
        this->_p.setDim( d + 1 );

        int i;
        GMlib::Vector<T, 4> basis = computeBasis(t, i);

        this->_p[0] = basis[0] * _c[i - 3] + basis[1] * _c[i - 2] + basis[2] * _c[i - 1] + basis[3] * _c[i];

        if( this->_dm == GMlib::GM_DERIVATION_EXPLICIT ) {

        }
    }

    template<typename T>
    T myBSpline<T>::getStartP() const
    {
        return _t[_d];
    }

    template<typename T>
    T myBSpline<T>::getEndP() const
    {
        return _t[_n];
    }

    template<typename T>
    int myBSpline<T>::findI(T t) const
    {
        int i;
        for (i = 0; i < _t.size(); i++) {
            if (_t[i] > t) {
                break;
            }
        }

        if (i > _n)
            i = _n;

        i--;

        return i;
    }

    template<typename T>
    T myBSpline<T>::findW(const int &d, const int &i, T t) const
    {
        return (t - _t[i]) / (_t[i+d] - _t[i]);
    }

    template<typename T>
    GMlib::Vector<T, 4> myBSpline<T>::computeBasis(T t, int& i) const
    {
        i = findI(t);
        auto a = (1 - findW(1, i, t))*(1- findW(2, i-1, t));
        auto b = ((1 - findW(1, i, t))*(findW(2, i-1, t))) + (findW(1, i, t) * (1 - findW(2, i, t)) );
        auto c = (findW(1, i, t) * findW(2, i, t));

        GMlib::Vector<T, 4> basis;
        basis[0] = a * (1 - findW(3, i-2, t));
        basis[1] = (a * (findW(3, i-2, t))) + (b * (1 - findW(3, i-1, t)) );
        basis[2] = (b * (findW(3, i-1, t)) + (c * (1 - findW(3, i, t))));
        basis[3] = c * (findW(3, i, t));

        return basis;
    }

    template<typename T>
    void myBSpline<T>::makeKnotVector(int n)
    {
        _t.resize(n+_k);
        for (int i = 0; i < _k; i++) {
            _t[i] = 0;
        }

        for (int i = _k; i <= n; i++) {
            _t[i] = i-_d;
        }

        for (int i = n+1; i < n+_k; i++) {
            _t[i] = _t[i-1];
        }

    }

    template<typename T>
    void myBSpline<T>::makeControlPoints(
            const GMlib::DVector<GMlib::Vector<T, 3>> &p)
    {
         auto m = p.getDim();

         // Set the dimensions of A
         GMlib::DMatrix<T> A(m, _n, T(0));

         // Find dt and compute A
         const T dt = (_t[_n] - _t[_d]) / (m - 1);
         GMlib::Vector<T, 4> basis;
         int i;
         for (int k = 0; k < m; k++) {
             basis = computeBasis(_t[_d] + k * dt, i);
             for (int l = i-_d; l <= i; l++) {
                 A[k][l] = basis[l-i+_d];
//                 qDebug() << "index[l-i+_d]" << l-i+_d;
//                 qDebug() << k << l;
             }
         }

//         qDebug() << A;

         // Find _c
         auto At = A;
         At.transpose();
         auto D = At * A;
         auto b = At * p;
         _c = D.invert() * b;

    }


}
