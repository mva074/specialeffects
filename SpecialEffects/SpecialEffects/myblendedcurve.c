#include "myblendedcurve.h"

template<typename T>
myBlendedCurve<T>::myBlendedCurve(GMlib::PCurve<T, 3> *c0, GMlib::PCurve<T, 3> *c1, T dt) :
    _dt(1 - dt), _d(3), _k(4), _c0(c0), _c1(c1)
{
    // blending requires dt between 0 and 1
    if (dt > 1) {
        _dt = T(1);
    }
    else if (dt < 0) {
        _dt = T(0);
    }

}

template<typename T>
myBlendedCurve<T>::~myBlendedCurve()
{

}

template<typename T>
void myBlendedCurve<T>::eval(T t, int d, bool l) const
{
    this->_p.setDim(d + 1);

    if (getStartP() <= t && t < _dt) {
        this->_p[0] = _c0->evaluate(t, 0)[0];
    } else if (_dt <= t && t < 1) {
        this->_p[0] = getPosBlend(t)[0];;
    } else if (t >= T(1) && t <= getEndP()) {
        this->_p[0] = _c1->evaluate(t - _dt, 0)[0];
    }

}

template<typename T>
T myBlendedCurve<T>::getStartP() const
{
    return T(-1);
}

template<typename T>
T myBlendedCurve<T>::getEndP() const
{
    return T(1) + _dt;
}

/**
 * Symmetric B-function:
 * Rational function of first order
 */

template<typename T>
T myBlendedCurve<T>::blend(T t) const
{
    return std::pow(t, 2) / (std::pow(t, 2) + std::pow((1-t), 2));
}

template<typename T>
GMlib::DVector<GMlib::Vector<T,3>> &myBlendedCurve<T>::getPosBlend(T t) const
{
    return _c0->evaluate(t, 0) + (blend(5*t - 4) * (_c1->evaluate(t - _dt, 0) - _c0->evaluate(t, 0)));
}




