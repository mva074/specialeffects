# Documentation
Documentation and general usage guide for project in STE6247 Applied Geometry and Special Effects.

## Getting Started
In order to run this project read following subheadings.

### Prerequisites
#### Libraries
 * GLEW can be found here: [GLEW Mainpage](http://glew.sourceforge.net/)

### Running this code
Follow the [setup guide](GMlibSetupGuide2018.pdf) to set up your project and get it running on your PC. After that, you can run the project in QtCreator.

#### Running the application
TODO

## Authors
**Daniel Aaron Salwerowicz** - *Developer and memer* - [CodeRefinery](https://source.coderefinery.org/MormonJesus69420)
